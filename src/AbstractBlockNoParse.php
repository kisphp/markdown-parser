<?php

namespace Kisphp;

abstract class AbstractBlockNoParse extends AbstractBlock
{
    /**
     * @return string
     */
    public function parse()
    {
        return $this->getStartTag() . $this->getEndTag();
    }

    /**
     * @return null|string
     */
    public function getStartTag()
    {
        return null;
    }

    /**
     * @return null|string
     */
    public function getEndTag()
    {
        return null;
    }
}
