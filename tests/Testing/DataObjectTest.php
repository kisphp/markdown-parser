<?php

namespace Kisphp\Testing;

use Kisphp\Exceptions\DataObjectBlockAlreadyExists;
use Kisphp\MarkdownFactory;
use PHPUnit\Framework\TestCase;

class DataObjectTest extends TestCase
{
    public function testDoubleAddTemplateBlock()
    {
        $this->expectException(DataObjectBlockAlreadyExists::class);

        $text = <<<MARKDOWN
:::code-1
this is my content
:::

:::code-1
this is my content
:::

MARKDOWN;

        $md = MarkdownFactory::createMarkdown();

        $md->parse($text);
    }
}
