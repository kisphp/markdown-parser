<?php

use Kisphp\BlockTypes;
use Kisphp\MarkdownFactory;
use PHPUnit\Framework\TestCase;

class BlockFactoryTest extends TestCase
{
    public function testCreateNoExistingBlock()
    {
        $this->expectException(\Kisphp\Exceptions\BlockNotFoundException::class);

        $bf = new MarkdownFactory();
        $bf->create('Alfa');
    }

    public function testBlockParagraph()
    {
        $factory = new MarkdownFactory();

        $this->assertInstanceOf(
            $factory->getClassNamespace(BlockTypes::BLOCK_PARAGRAPH),
            $factory->create(BlockTypes::BLOCK_PARAGRAPH)
        );
    }

    /**
     * @group Dummy
     */
    public function testAddedCustomBlock()
    {
        $md = \Kisphp\Testing\Dummy\DummyFactory::createMarkdown();

        $this->assertSame('<span>custom block</span>', $md->parse('^ custom block'));
    }

    public function testWrongPlugin()
    {
        $this->expectException(\Kisphp\Exceptions\ParameterNotAllowedException::class);

        $md = \Kisphp\Testing\Dummy\DummyFactory::createMarkdown();

        $factory = $md->getFactory();

        $factory->addBlockPlugin('^', new stdClass());
    }

    public function testAddSameBlockType()
    {
        $md = \Kisphp\Testing\Dummy\DummyFactory::createMarkdown();

        $factory = $md->getFactory();
        $factory->addBlockPlugin('^', 'BlockDummy');
        $factory->addBlockPlugins('^', ['BlockDummy', 'BlockParagraph']);

        $this->assertEquals(2, count($factory->getBlockPlugins()['^']));
    }

    public function testAddPluginsFromMarkdown()
    {
        $md = \Kisphp\Testing\Dummy\DummyFactory::createMarkdown();
        $md->addRules('^', ['BlockDummy', 'BlockParagraph']);

        $this->assertEquals(2, count($md->getFactory()->getBlockPlugins()['^']));
    }
}
