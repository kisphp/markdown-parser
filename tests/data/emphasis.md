Emphasis, aka italics, with *asterisks* or _underscores_.

Combined emphasis with **asterisks and _underscores_**.

Emphasis with __type *underscores*__.
Emphasis with **type _asterisks_**.

and _ this should not be _ emphasized 1

and \_this should not be\_ emphasized 2

and \*this should not be\* emphasized 3

and * this should not be * emphasized 4

and ** another one ** here

and __ another one __ here

have this _italic_ text and _this one_ on the same *line*.

have this *italic* text and *this one* on the same _line_.