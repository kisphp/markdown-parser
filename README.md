# Kisphp Markdown Parser

[![pipeline status](https://gitlab.com/kisphp/markdown-parser/badges/master/pipeline.svg)](https://gitlab.com/kisphp/markdown-parser/-/commits/master)
[![coverage report](https://gitlab.com/kisphp/markdown-parser/badges/master/coverage.svg)](https://gitlab.com/kisphp/markdown-parser/-/commits/master)

[![Latest Stable Version](https://poser.pugx.org/kisphp/markdown-parser/v/stable)](https://packagist.org/packages/kisphp/markdown-parser)
[![Total Downloads](https://poser.pugx.org/kisphp/markdown-parser/downloads)](https://packagist.org/packages/kisphp/markdown-parser)
[![License](https://poser.pugx.org/kisphp/markdown-parser/license)](https://packagist.org/packages/kisphp/markdown-parser)
[![Monthly Downloads](https://poser.pugx.org/kisphp/markdown-parser/d/monthly)](https://packagist.org/packages/kisphp/markdown-parser)

## What is this ?

A highly extensible and customizable PHP Markdown Parser that converts makrdown format into HTML format.
Parsing Markdown to HTML is as simple as calling a single method `$markdown->parse($markdownContent)` (see [Usage](https://gitlab.com/kisphp/markdown-parser/-/wikis/home)).
To extend the Markdown class to parse custom blocks or format, is as simple as creating new classes that implements `BlockInterface` and include them in the system.
For this, please see [How to extend blocks](https://gitlab.com/kisphp/markdown-parser/-/wikis/Blocks-Extension-Points).

## What has different from other markdown parsers ?

- code templates to be inserted in other code blocks structures
- generate tables without headers
- very easy mode to extend

## Useful wiki pages

- [Installation &amp; Usage](https://gitlab.com/kisphp/markdown-parser/-/wikis/home)
- [Templates usage](https://gitlab.com/kisphp/markdown-parser/-/wikis/Template-blocks)
- [How to extend blocks](https://gitlab.com/kisphp/markdown-parser/-/wikis/Blocks-Extension-Points)
